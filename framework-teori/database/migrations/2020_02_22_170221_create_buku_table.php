<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBukuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku', function (Blueprint $table) {
            $table->bigIncrements('id')->length(5)->unsigned();
            $table->string('judul', 75);
            $table->char('tahun_terbit', 4);
            $table->integer('id_penerbit');
            $table->string('pengarang', 100);
            $table->integer('jumlah_hal')->length(4);
            $table->string('sampul', 30);
            $table->integer('id_user')->default(0);
            $table->timestamps();
        });
        DB::update("ALTER TABLE buku AUTO_INCREMENT = 10001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku');
    }
}

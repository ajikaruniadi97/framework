<?php

use Illuminate\Database\Seeder;

class KotaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kota')->insert([
            ['propinsi_id'=>1,'nama_kota'=>'Kodya Yogyakarta'],
            ['propinsi_id'=>1,'nama_kota'=>'bantul'],
            ['propinsi_id'=>2,'nama_kota'=>'kota gede']
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(KotaTableSeeder::class);
        $this->call(PenerbitTableSeeder::class);
        $this->call(BukuTableSeeder::class);
        $this->call(ProvinsiSeeder::class);
        $this->call(BarangSeeder::class);

    }
}

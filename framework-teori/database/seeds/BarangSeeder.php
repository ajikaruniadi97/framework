<?php

use Illuminate\Database\Seeder;

class BarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('barang')->insert([
            ['jenis_id'=>1,'nama_barang'=>'Air Mineral', 'satuan'=>'botol', 'harga'=>5000, 'stok'=>7, 'user_id'=>1],
            ['jenis_id'=>2,'nama_barang'=>'Minyak Goreng', 'satuan'=>'pcs', 'harga'=>20000, 'stok'=>16, 'user_id'=>1],
            ['jenis_id'=>1,'nama_barang'=>'Pengganris', 'satuan'=>'batang', 'harga'=>5000, 'stok'=>29, 'user_id'=>1],
            ['jenis_id'=>1,'nama_barang'=>'Buku Bergaris', 'satuan'=>'lb', 'harga'=>5000, 'stok'=>8, 'user_id'=>1],
            ['jenis_id'=>1,'nama_barang'=>'Spidol', 'satuan'=>'batang', 'harga'=>5000, 'stok'=>200, 'user_id'=>1],
            ['jenis_id'=>2,'nama_barang'=>'gula putih', 'satuan'=>'kg', 'harga'=>10000, 'stok'=>10, 'user_id'=>1],
            ['jenis_id'=>1,'nama_barang'=>'buku gambar', 'satuan'=>'pcs', 'harga'=>10000, 'stok'=>10, 'user_id'=>1],
            ['jenis_id'=>1,'nama_barang'=>'buku bacaan', 'satuan'=>'pcs', 'harga'=>10000, 'stok'=>10, 'user_id'=>1],
            ['jenis_id'=>1,'nama_barang'=>'kertas A4', 'satuan'=>'rem', 'harga'=>30000, 'stok'=>2, 'user_id'=>1],
            ['jenis_id'=>1,'nama_barang'=>'buku gambar mewarnai', 'pcs'=>'pcs', 'harga'=>10000, 'stok'=>10, 'user_id'=>1],
            ['jenis_id'=>2,'nama_barang'=>'Kopi mix', 'satuan'=>'saset', 'harga'=>2500, 'stok'=>1000, 'user_id'=>1],
            ['jenis_id'=>1,'nama_barang'=>'buku tulis', 'satuan'=>'pcs', 'harga'=>10000, 'stok'=>10, 'user_id'=>1],
        ]);
    }
}

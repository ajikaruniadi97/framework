<?php

use Illuminate\Database\Seeder;

class BukuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buku')->insert([
            ['judul'=>'menjadi jempolan programer web php', 'tahun_terbit'=>'2018', 'id_penerbit'=>'1', 'pengarang'=>'badiyanto', 'jumlah_hal'=>'400', 'sampul'=>'sampul'],
            ['judul'=>'simulasi arduino', 'tahun_terbit'=>'2017', 'id_penerbit'=>'1', 'pengarang'=>'darmanto', 'jumlah_hal'=>'300', 'sampul'=>'sampul'],
            ['judul'=>'algoritma dan pemrograman', 'tahun_terbit'=>'2017', 'id_penerbit'=>'3', 'pengarang'=>'suryanto', 'jumlah_hal'=>'300', 'sampul'=>'sampul'],
            ['judul'=>'buku pintar framework yii', 'tahun_terbit'=>'2019', 'id_penerbit'=>'5', 'pengarang'=>'badiyanto', 'jumlah_hal'=>'300', 'sampul'=>'sampul'],
            ['judul'=>'buku pintar framework yii', 'tahun_terbit'=>'2019', 'id_penerbit'=>'5', 'pengarang'=>'badiyanto', 'jumlah_hal'=>'300', 'sampul'=>'sampul'],
        ]);
    }
}

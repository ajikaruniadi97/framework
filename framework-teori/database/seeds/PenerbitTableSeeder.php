<?php

use Illuminate\Database\Seeder;

class PenerbitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('penerbit')->insert([
            ['penerbit'=>'Graha Ilmu', 'alamat'=>'Ruko Jambusari No. 7A, Wadomartani', 'telepon'=>'1234567', 'email'=>'pesanan@graha.com'],
            ['penerbit'=>'Jaya pres', 'alamat'=>'Jln. Wonosari', 'telepon'=>'1234567', 'email'=>'kirim@jaya.com'],
            ['penerbit'=>'MediaKom', 'alamat'=>'Deresan CT X, Yogyakarta', 'telepon'=>'1234567', 'email'=>'kirim@jaya.com'],
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kota;

class KotaController extends Controller
{
    public function index(){
        $kota = Kota::paginate(2);

        return json_encode($kota);
    }

    public function create(Request $request){
        $kota = new Kota;
        $kota->propinsi_id = $request->propinsi_id;
        $kota->nama_kota = $request->nama_kota;
        $kota->save();

        return json_encode($kota);
    }

    public function update(Request $request, $id){
        $kota = Kota::find($id);
        $kota->propinsi_id = $request->propinsi_id;
        $kota->nama_kota = $request->nama_kota;
        $kota->save();

        return json_encode($kota);
    }

    public function delete($id){
        $kota = Kota::find($id);
        $kota->delete();

        return response()->json([
            'messages' => 'data berhasil di hapus',
        ]);
    }

    public function getById($id){
        $kota = Kota::find($id);
        return json_encode($kota);
    }
}

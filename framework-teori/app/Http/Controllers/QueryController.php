<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QueryController extends Controller
{
    public function getKota(){
        // query pertama
        $kota = \DB::table('kota')->get();

        foreach ($kota as $kotas){
            echo $kotas->id."->".$kotas->nama_kota;
            echo "<br>";
            echo "<hr>";
        }

        // query kedua
        $kota = \DB::table('kota')->where('nama_kota', 'bantul')->first();
            echo $kota->id."->".$kota->nama_kota;
            echo "<br>";
            echo "<hr>";

        // query ketiga
        $propId = \DB::table('kota')->where('nama_kota', 'bantul')->value('propinsi_id');
            echo "Propinsi_id = ".$propId;
            echo "<br>";
            echo "<hr>";

        // query keempat
        $kota = \DB::table('kota')->where('propinsi_id', 1)->pluck('nama_kota');
            foreach ($kota as $kotas){
                echo $kotas;
                echo "<br>";
                echo "<hr>";
            }

        // query kelima
        $kota = \DB::table('kota')->pluck('nama_kota','id');
            echo json_encode ($kota);
            echo "<br>";
            foreach ($kota as $key=>$nama_kota){
                echo $key." -> ".$nama_kota;
                echo "<br>";
            }
            echo "<hr>";

        // query keenam
        $kota = \DB::table('kota')->orderBy('id')->chunk(100, function($kota){
            foreach ($kota as $kotas){
                echo $kotas->id." -> ".$kotas->nama_kota;
                echo "<br>";
            }
        });

    }
}

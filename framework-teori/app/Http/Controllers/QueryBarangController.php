<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QueryBarangController extends Controller
{
    public function getBarang(){
        // query pertama
        $jRec = \DB::table('barang')->count();
        echo "Jumlah Macam Barang = ".$jRec." Rekaman";
        echo "<hr>";

        // query kedua
        $hargaMax = \DB::table('barang')->where('jenis_id', 1)->max('harga');
        $hargaMin = \DB::table('barang')->where('jenis_id', 1)->min('harga');
        echo "Harga Max barang = Rp. ".$hargaMax;
        echo "<br>";
        echo "Harga min barang = Rp. ".$hargaMin;
        echo "<hr>";

        // query ketiga
        $hargaAvg = \DB::table('barang')->where('jenis_id', 1)->avg('harga');
        $hargaNet = \DB::table('barang')->where('jenis_id', 1)->sum(\DB::raw('stok*harga'));
        echo "Harga Avg barang = Rp. ".$hargaAvg;
        echo "<br>";
        echo "Jumlah Harga barang = Rp. ".$hargaNet;
        echo "<hr>";

        // query kelima
        var_dump(\DB::table('barang')->where('id', 1)->exists());
        var_dump(\DB::table('barang')->where('id', 1)->doesntExist());
        echo "<hr>";

        // query keenam
        $barang = \DB::table('barang')->select('id', 'nama_barang')->get();
            foreach ($barang as $b)
            {
                echo $b->id.":".$b->nama_barang;
                echo "<br>";
            }
        echo "<hr>";

        // query ketujuh
        $barang = \DB::table('barang')->select('jenis_id')->distinct('jenis_id')->get();
            foreach ($barang as $b)
            {
                echo $b->jenis_id; 
                echo "<br>";
            }
        echo "<hr>";

        // query kedelapan
        $barang = \DB::table('barang')->select('jenis_id')->addSelect('id','nama_barang')->where('jenis_id',2)->get();
            foreach ($barang as $b){
                echo $b->jenis_id.":".$b->id.":".$b->nama_barang;
                echo "<br>";
            }
        echo "<hr>";

        // query ke sembilan
        $barang = \DB::table('barang')->select(\DB::raw("id,nama_barang, harga+harga*10/100 as harga_sekarang"))->get();
            foreach ($barang as $b){
                echo $b->id ."\t". $b->nama_barang. "\t Rp " .
                number_format($b->harga_sekarang);
                echo "<br>";
            }
        echo "<hr>";

        // query ke 10
        $barang = \DB::table('barang')->select(\DB::raw("id,nama_barang, harga+harga*10/100 as harga_sekarang")) ->where('harga','<',10000)->get();
        foreach ($barang as $b){
            echo $b->id ."\t". $b->nama_barang. "\t Rp " .
            number_format($b->harga_sekarang);
            echo "<br>";
        }
        echo "<hr>";

        // query ke 11
        $barang = \DB::table('barang')->selectRaw('id, nama_barang, harga * ? as harga_plus_ppn',[1.01])->limit(5)->get();
            foreach ($barang as $b){
                echo $b->id ."\t". $b->nama_barang. "\t Rp ".number_format($b->harga_plus_ppn);
                echo "<br>"; 
            }
        echo "<hr>";

        $barang = \DB::table('barang')->select('jenis_id',\DB::raw('SUM(harga) as total_harga'))->groupBy('jenis_id') ->havingRaw('SUM(harga) > ?', [5000]) ->get();
            foreach ($barang as $b){
                echo $b->jenis_id."\t Rp " .number_format($b->total_harga);
                echo "<br>";
        }

        $barang = \DB::table('barang')
                ->whereRaw('harga >= ?',[10000])
                ->orderByRaw('nama_barang DESC')
                ->get();
            foreach ($barang as $b){
                echo $b->id.":".$b->nama_barang.":".$b->harga;
                echo "<br>";
            }
    }
}
